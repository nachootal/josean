# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'josean/version'

Gem::Specification.new do |spec|
  spec.name          = 'josean'
  spec.version       = Josean::VERSION
  spec.authors       = ['Nacho']
  spec.email         = ['nachootal@gmail.com']

  spec.summary       = 'Implements the JSON layouts for Jasonette https://docs.jasonette.com.'
  spec.description   = 'Implements the JSON layouts for Jasonette https://docs.jasonette.com.'
  spec.homepage      = 'https://bitbucket.org/nachootal/josean'
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end

  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler'
  spec.add_development_dependency 'byebug'
  spec.add_development_dependency 'faker'
  spec.add_development_dependency 'minitest'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'simplecov'
  spec.add_runtime_dependency 'json'
end
