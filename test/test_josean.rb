# frozen_string_literal: true
require "test_helper"
require "josean"

class JoseanTest < Minitest::Test
  def setup
    @empty_app = "{\"$jason\":{\"head\":{},\"body\":{\"sections\":[]}}}"
    @valid_style = {background: "#f0ddaa", padding: "20", color: "#5ebdb2"}
  end

  def test_head
    app = Josean::Application.new(Faker::Internet.url)
    assert_equal(app.to_json, @empty_app)
    title = Faker::Lorem.word
    description = Faker::Lorem.sentence
    icon = Faker::LoremFlickr.image
    valid_head = "{\"$jason\":{\"head\":{\"title\":\"#{title}\",\"description\":\"#{description}\",\"icon\":\"#{icon}\",\"style\":#{JSON.unparse(@valid_style)}},\"body\":{\"sections\":[]}}}"
    app.head(title, description, icon, @valid_style)
    assert_equal(valid_head, app.to_json)
  end

  def test_body_header
    app = Josean::Application.new(Faker::Internet.url)
    assert_equal(app.to_json, @empty_app)
    image = Faker::LoremFlickr.image
    app.body_header(image, @valid_style, @valid_style)
    valid_body_header = "{\"$jason\":{\"head\":{},\"body\":{\"sections\":[],\"header\":{\"title\":{\"type\":\"image\",\"url\":\"#{image}\",\"style\":#{JSON.unparse(@valid_style)}},\"style\":#{JSON.unparse(@valid_style)}}}}}"
    assert_equal(valid_body_header, app.to_json)
  end

  def body_style
    app = Josean::Application.new(Faker::Internet.url)
    assert_equal(app.to_json, @empty_app)
    app.body_style(@valid_style)
    assert_equal("", app.to_json)
  end

  def test_section
    app = Josean::Application.new(Faker::Internet.url)
    assert_equal(app.to_json, @empty_app)
    new_section = app.section
    empty_items = []
    assert_equal(empty_items, new_section)
    [app, new_section]
  end

  def test_section_item
    app, new_section = test_section
    title = Faker::Lorem.word
    test_item = Josean::Objects.label(title, @valid_style)
    app.section_item(new_section, test_item)
    valid_response = "{\"$jason\":{\"head\":{},\"body\":{\"sections\":[{\"items\":[{\"type\":\"label\",\"text\":\"#{title}\",\"style\":#{JSON.unparse(@valid_style)}}]}]}}}"
    assert_equal(valid_response, app.to_json)
    assert_nil(app.section_item(app, test_item))
    [app, valid_response]
  end

  def test_scrolled_section_component
    app, new_section = test_scrolled_section(@valid_style)
    title = Faker::Lorem.word
    test_component = Josean::Objects.label(title, @valid_style)
    app.scrolled_section_component(new_section, test_component)
    valid_response = "{\"$jason\":{\"head\":{},\"body\":{\"sections\":[{\"items\":[{\"type\":\"vertical\",\"style\":#{JSON.unparse(@valid_style)},\"components\":[{\"type\":\"label\",\"text\":\"#{title}\",\"style\":#{JSON.unparse(@valid_style)}}]}]}]}}}"
    assert_equal(valid_response, app.to_json)
    app.scrolled_section_component(new_section, test_component)
    valid_response = "{\"$jason\":{\"head\":{},\"body\":{\"sections\":[{\"items\":[{\"type\":\"vertical\",\"style\":#{JSON.unparse(@valid_style)},\"components\":[{\"type\":\"label\",\"text\":\"#{title}\",\"style\":#{JSON.unparse(@valid_style)}},{\"type\":\"label\",\"text\":\"#{title}\",\"style\":#{JSON.unparse(@valid_style)}}]}]}]}}}"
    assert_equal(valid_response, app.to_json)
    [app, valid_response]
  end

  def test_scrolled_sections_and_normal_sections_combined
    app, = test_scrolled_section_component
    new_section = app.section
    empty_items = []
    assert_equal(empty_items, new_section)
    title = Faker::Lorem.word
    test_item = Josean::Objects.label(title, @valid_style)
    app.section_item(new_section, test_item)
  end

  def test_normal_sections_and_scrolled_sections_combined
    app, = test_section_item
    new_section = app.scrolled_section("vertical", @valid_style)
    title = Faker::Lorem.word
    test_component = Josean::Objects.label(title, @valid_style)
    app.scrolled_section_component(new_section, test_component)
  end

  def test_get_request_action
    app_url = Faker::Internet.url
    app = Josean::Application.new(app_url)
    assert_equal(app.to_json, @empty_app)
    title = Faker::Lorem.word
    url = Faker::Internet.url
    view = Faker::Internet.slug
    _ = app.section
    app.get_request_action(title, url, view)
    valid_response = "{\"$jason\":{\"head\":{\"actions\":{\"#{title}\":{\"type\":\"$network.request\",\"options\":{\"url\":\"#{url}\",\"method\":\"get\",\"data\":null},\"success\":{\"type\":\"href\",\"options\":{\"url\":\"#{app_url + "/" + view}\",\"transtion\":\"replace\"}},\"error\":{\"type\":\"$util.toast\",\"options\":{\"text\":\"Something went wrong when GET on #{url}.\",\"type\":\"Error\"}}}}},\"body\":{\"sections\":[{\"items\":[]}]}}}"
    assert_equal(valid_response, app.to_json)
  end

  def test_post_request_action
    app_url = Faker::Internet.url
    app = Josean::Application.new(app_url)
    assert_equal(app.to_json, @empty_app)
    title = Faker::Lorem.word
    url = Faker::Internet.url
    view = Faker::Internet.slug
    _ = app.section
    data_hash = {name: Faker::Name.first_name, surname: Faker::Name.last_name}
    app.post_request_action(title, url, data_hash, view)
    valid_response = "{\"$jason\":{\"head\":{\"actions\":{\"#{title}\":{\"type\":\"$network.request\",\"options\":{\"url\":\"#{url}\",\"method\":\"post\",\"data\":#{JSON.unparse(data_hash)}},\"success\":{\"type\":\"href\",\"options\":{\"url\":\"#{app_url + "/" + view}\",\"transtion\":\"replace\"}},\"error\":{\"type\":\"$util.toast\",\"options\":{\"text\":\"Something went wrong when POST on #{url}.\",\"type\":\"Error\"}}}}},\"body\":{\"sections\":[{\"items\":[]}]}}}"
    assert_equal(valid_response, app.to_json)
  end

  def test_put_request_action
    app_url = Faker::Internet.url
    app = Josean::Application.new(app_url)
    assert_equal(app.to_json, @empty_app)
    title = Faker::Lorem.word
    url = Faker::Internet.url
    view = Faker::Internet.slug
    data_hash = {name: Faker::Name.first_name, surname: Faker::Name.last_name}
    _ = app.section
    app.put_request_action(title, url, data_hash, view)
    valid_response = "{\"$jason\":{\"head\":{\"actions\":{\"#{title}\":{\"type\":\"$network.request\",\"options\":{\"url\":\"#{url}\",\"method\":\"put\",\"data\":#{JSON.unparse(data_hash)}},\"success\":{\"type\":\"href\",\"options\":{\"url\":\"#{app_url + "/" + view}\",\"transtion\":\"replace\"}},\"error\":{\"type\":\"$util.toast\",\"options\":{\"text\":\"Something went wrong when PUT on #{url}.\",\"type\":\"Error\"}}}}},\"body\":{\"sections\":[{\"items\":[]}]}}}"
    assert_equal(valid_response, app.to_json)
  end

  def test_to_json
    app_url = Faker::Internet.url
    app = Josean::Application.new(app_url)
    assert_kind_of(String, app.to_json)
    assert_includes(app.to_json, "$jason")
    assert_includes(app.to_json, "head")
    assert_includes(app.to_json, "body")
    assert_includes(app.to_json, "sections")
  end

  private

  def test_scrolled_section(style)
    app = Josean::Application.new(Faker::Internet.url)
    assert_equal(app.to_json, @empty_app)
    new_section = app.scrolled_section("vertical", style)
    assert(new_section.is_a?(Array))
    assert(new_section.first.keys.include?(:type))
    assert(new_section.first.keys.include?(:style))
    assert(new_section.first.keys.include?(:components))
    assert(new_section.first[:type].is_a?(String))
    assert(new_section.first[:style].is_a?(Hash))
    assert(new_section.first[:components].is_a?(Array))
    [app, new_section]
  end
end
