# frozen_string_literal: true
require "minitest/autorun"
require "byebug"
require "faker"
require "simplecov"


SimpleCov.start do
  add_filter("/test/")
end

module JoseanTestHelper
end
