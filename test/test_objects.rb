# frozen_string_literal: true
require "test_helper"
require "josean"
require "josean/objects"

class JoseanObjectsTest < Minitest::Test
  # Setup of the test containing a set of valid and invalid parameters and the correct output
  def setup
    @object = Josean::Objects
    @valid_name = Faker::Name.first_name
    @invalid_name = Faker::Number.number(    digits: 1).to_i
    @valid_url = Faker::Internet.url
    @invalid_url = Faker::Lorem.word
    @valid_style = {background: "#f0ddaa", padding: "20", color: "#5ebdb2"}
    @valid_style_space = {background: "#0f0ddaa", widht: "12px", height: "15px"}
    @invalid_style_space = {padding: "20"}
    @invalid_style = Faker::Lorem.word
    @valid_value_switch = true
    @invalid_value_switch = Faker::Number.number(    digits: 1).to_i
    @valid_value_slider = Faker::Number.between(    from: 0,     to: 100).to_f / 100
    @invalid_value_slider = Faker::Number.negative.to_i
    @valid_coordinates = [
      Faker::Number.decimal(      l_digits: 2,       r_digits: 7).to_f,
      Faker::Number.decimal(      l_digits: 2,       r_digits: 7).to_f,
    ]
    @valid_coordinates_output = @valid_coordinates.join(",")
    @invalid_coordinates = Faker::Lorem.word
    @valid_value_radio = Faker::Number.decimal(    l_digits: 5,     r_digits: 1).to_f
    @valid_value_radio_output = @valid_value_radio.to_s
    @invalid_value_radio = Faker::Lorem.word
    @valid_pin = {
      title: "This is a pin",
      description: "Truly a pin",
      coord: @valid_coordinates,
      style: @valid_style,
    }
    @valid_pin_output = {
      title: "This is a pin",
      description: "Truly a pin",
      coord: @valid_coordinates_output,
      style: @valid_style,
    }
    @valid_pins = [@valid_pin, @valid_pin]
    @valid_pins_output = [
      {
        title: "This is a pin",
        description: "Truly a pin",
        coord: @valid_coordinates_output,
        style: @valid_style,
      },
      {
        title: "This is a pin",
        description: "Truly a pin",
        coord: @valid_coordinates_output,
        style: @valid_style,
      },
    ]
    @invalid_pin = {
      title: 43,
      description: 24,
      coord: @invalid_coordinates,
      style: @invalid_style,
    }
    @invalid_pins = [@invalid_pin, @invalid_pin]
    @valid_action = {
      type: "$network.request",
      options: {url: "https://jasonbase.com/things/jYJ.json"},
      success: {type: "$render"},
    }
    @invalid_action = "Invalid action"
    @valid_label = {type: "label", text: @valid_name, style: @valid_style}
    @valid_textfield = {
      type: "textfield",
      name: @valid_name,
      keyboard: "email",
      placeholder: "Enter #{@valid_name.downcase}",
      style: @valid_style,
    }
    @valid_switch = {
      type: "switch",
      name: @valid_name,
      value: @valid_value_switch,
      action: {
        trigger: @valid_action,
        options: {item: "Name of the switch #{@valid_name}"},
      },
    }
    @valid_image = {type: "image", url: @valid_url, style: @valid_style}
    @valid_slider = {
      type: "slider",
      name: @valid_name,
      value: @valid_value_slider.to_s,
      action: {trigger: @valid_action},
    }
    @valid_space = {type: "space", style: @valid_style_space}
    @valid_map = {
      type: "map",
      region: {
        coord: @valid_coordinates_output,
        width: @valid_value_radio_output,
        height: @valid_value_radio_output,
      },
      pins: @valid_pins_output,
      style: @valid_style,
    }
    @valid_map_no_pins = {
      type: "map",
      region: {
        coord: @valid_coordinates_output,
        width: @valid_value_radio_output,
        height: @valid_value_radio_output,
      },
      style: @valid_style,
    }
  end

  # Test for switch component https://docs.jasonette.com/components/#switch
  def test_switch
    [@valid_name, @invalid_name].each do |name|
      [@valid_value_switch, @invalid_value_switch].each do |value|
        [@valid_action, @invalid_action].each do |action|
          [@valid_style, @invalid_style].each do |style|
            if name == @valid_name && value == @valid_value_switch && action == @valid_action && style == @valid_style
              assert_equal(@object.switch(name, value, action, style), @valid_switch)
            else
              assert_nil(@object.switch(name, value, action, style))
            end
          end
        end
      end
    end
  end

  # Test for label component https://docs.jasonette.com/components/#label
  def test_label
    [@valid_name, @invalid_name].each do |name|
      [@valid_style, @invalid_style].each do |style|
        if name == @valid_name && style == @valid_style
          assert_equal(@object.label(name, style), @valid_label)
        else
          assert_nil(@object.label(name, style))
        end
      end
    end
  end

  # Test for textfield component https://docs.jasonette.com/components/#textfield
  def test_textfield
    [@valid_name, @invalid_name].each do |name|
      [@valid_style, @invalid_style].each do |style|
        if name == @valid_name && style == @valid_style
          assert_equal(@object.textfield(name, style), @valid_textfield)
        else
          assert_nil(@object.textfield(name, style))
        end
      end
    end
  end

  # Test for button component https://docs.jasonette.com/components/#button
  def test_button
    valid_url = Faker::Internet.url
    invalid_url = "invalid url"

    [@valid_name, @invalid_name].each do |name|
      [valid_url, invalid_url].each do |url|
        [@valid_style, @invalid_style].each do |style|
          if name == @valid_name && url == valid_url && style == @valid_style
            assert_equal(@object.button(name, url, style), {
              type: "label",
              text: name,
              style: style,
              :"href" => {:"url" => url},
            })
          else
            assert_nil(@object.button(name, url, style))
          end
        end
      end
    end
  end

  # Test for image component https://docs.jasonette.com/components/#image
  def test_image
    [@valid_url, @invalid_url].each do |url|
      [@valid_style, @invalid_style].each do |style|
        if url == @valid_url && style == @valid_style
          assert_equal(@object.image(url, style), @valid_image)
        else
          assert_nil(@object.image(url, style))
        end
      end
    end
  end

  # Test for slider component https://docs.jasonette.com/components/#slider
  def test_slider
    [@valid_name, @invalid_name].each do |name|
      [@valid_value_slider, @invalid_value_slider].each do |value|
        [@valid_action, @invalid_action].each do |action|
          [@valid_style, @invalid_style].each do |style|
            if name == @valid_name && value == @valid_value_slider && action == @valid_action && style == @valid_style
              assert_equal(@object.slider(name, value, action, style), @valid_slider)
            else
              assert_nil(@object.slider(name, value, action, style))
            end
          end
        end
      end
    end
  end

  # Test for space component https://docs.jasonette.com/components/#space
  def test_space
    assert_equal(@object.space(@valid_style_space), @valid_space)
    assert_nil(@object.space(@invalid_style_space))
  end

  # Test for pin component for a map https://docs.jasonette.com/components/#map
  def test_pin
    [@valid_pins, [], @invalid_pins].each do |pins|
      pins.each do |p|
        if p == @valid_pin
          assert_equal(@valid_pin_output, @object.pin(p[:title], p[:description], p[:coord], p[:style]))
        else
          assert_nil(@object.pin(p[:title], p[:description], p[:coord], p[:style]))
        end
      end
    end
  end

  # Returns true if the settings are for a valid map
  def valid_map_settings?(coordinates, width, height, pins, style)
    (coordinates == @valid_coordinates) && (width == @valid_value_radio) && (height == @valid_value_radio) && (pins == @valid_pins) && (style == @valid_style)
  end

  # Returns true if the settings are for a valid map without pins
  def valid_map_without_pins_settings?(coordinates, width, height, pins, style)
    (coordinates == @valid_coordinates) && (width == @valid_value_radio) && (height == @valid_value_radio) && (pins != @valid_pins) && (style == @valid_style)
  end

  # Test for map component https://docs.jasonette.com/components/#map
  def test_map
    [@valid_coordinates, @invalid_coordinates].each do |coordinates|
      [@valid_value_radio, @invalid_value_radio].each do |width|
        [@valid_value_radio, @invalid_value_radio].each do |height|
          [@valid_pins, [], @invalid_pins].each do |pins|
            [@valid_style, @invalid_style].each do |style|
              assert_equal(@valid_map, @object.map(coordinates, width, height, pins, style)) if valid_map_settings?(coordinates, width, height, pins, style)
              assert_equal(@valid_map_no_pins, @object.map(coordinates, width, height, pins, style)) if valid_map_without_pins_settings?(coordinates, width, height, pins, style)
              assert_nil(@object.map(coordinates, width, height, pins, style)) unless valid_map_settings?(coordinates, width, height, pins, style) || valid_map_without_pins_settings?(coordinates, width, height, pins, style)
            end
          end
        end
      end
    end
  end
end
