# frozen_string_literal: true
require "josean/version"
require "josean/objects"

module Josean
  ##
  # This class describes a jasonette application
  class Application
    ##
    # Initializes an application that is to be served from a certain URL
    # application_url The base url from which the application is served
    def initialize(application_url)
      @application_url = application_url
      @total_configuration = {}
      @total_configuration[:head] = {}
      @total_configuration[:body] = {}
      @total_configuration[:body][:sections] = []
    end

    ##
    # Sets the head of the application
    # title  Title of the application
    # description Description of the application
    # icon URL to the icon image
    # style Hash containing the CSS style
    def head(title, description, icon, style)
      @total_configuration[:head] = {title: title, description: description, icon: icon, style: style}
    end

    ##
    # Sets the body header of the application
    # image URL to the image
    # style_image Hash containing the CSS style of the image
    # style Hash containing the CSS style of the frame
    def body_header(image, style_image, style)
      @total_configuration[:body][:header] = {
        title: {type: "image", url: image, style: style_image},
        style: style,
      }
    end

    ##
    # Add a body style section
    # style Hash containing the CSS style of the frame
    def body_style(style)
      @total_configuration[:body][:style] = style
    end

    # Add a body section and return a handler for it
    def section
      @total_configuration[:body][:sections] << {items: []}
      @total_configuration[:body][:sections].last[:items]
    end

    ##
    # Add a body section that can scroll components and return a handler for it
    def scrolled_section(type, style)
      return unless valid_type?(type) && style.is_a?(Hash)
      @total_configuration[:body][:sections] << {items: []}
      @total_configuration[:body][:sections].last[:items] << {type: type, style: style, components: []}
      @total_configuration[:body][:sections].last[:items]
    end

    ##
    # Add an item to a given section
    # section handler to which section
    # item Item to be added. It needs to be a Jasonette object
    def section_item(section, item)
      section << item if section.is_a?(Array)
    end

    ##
    # Add a component to a given scrolled section
    # section handler to which section
    # component Component to be added. It needs to be a Jasonette object
    def scrolled_section_component(section, component)
      section.last[:components] << component if section.is_a?(Array) && section.last.keys.include?(:components)
    end

    ##
    # Add a get call with a given action name
    # name_of_resource Name of the resource that this request will be attached to
    # url URL to which the request will be issued
    # view View to which the application will be transitioned on success
    def get_request_action(name_of_resource, url, view)
      create_network_request("get", name_of_resource, url, nil, view)
    end

    ##
    # Add a post call with a given action name
    # name_of_resource Name of the resource that this request will be attached to
    # url URL to which the request will be issued
    # view View to which the application will be transitioned on success
    def post_request_action(name_of_resource, url, data_hash, view)
      create_network_request("post", name_of_resource, url, data_hash, view)
    end

    ##
    # Add a put call with a given action name
    # name_of_resource Name of the resource that this request will be attached to
    # url URL to which the request will be issued
    # view View to which the application will be transitioned on success
    def put_request_action(name_of_resource, url, data_hash, view)
      create_network_request("put", name_of_resource, url, data_hash, view)
    end

    ##
    # Returns the json so that it can be served
    def to_json
      "{\"$jason\":" + JSON.unparse(@total_configuration) + "}" if valid?
    end

    private

    ##
    # Checks whether the configuration is correct
    def valid?
      return_value = false

      unless @total_configuration.keys.length != 2 && @total_configuration[:head].empty? || @total_configuration[:body].empty?
        return_value = valid_header? && valid_body?
      end

      return_value
    end

    ##
    # Checks wether the header is valid
    def valid_header?
      check_content([
        [:title, String],
        [:description, String],
        [:icon, String],
        [:offline, String],
        [:styles, Hash],
        [:actions, Hash],
        [:templates, Hash],
        [:data, Hash],
        [:agents, Hash],
      ], :head)
    end

    ##
    # Checks wether the body is valid
    def valid_body?
      check_content([
        [:header, Hash],
        [:sections, Array],
        [:layers, Array],
        [:footer, Hash],
      ], :body)
    end

    ##
    # Checks wether the type of a section is valid
    def valid_type?(type)
      ["vertical", "horizontal"].include?(type)
    end

    ##
    # Checks that the array of tags and content is correct
    def check_content(list, region_of_configuration)
      list.map do |k|
        @total_configuration[region_of_configuration].keys.include?(k.first) ? @total_configuration[region_of_configuration][k.first].is_a?(k.last) : true
      end.include?(false) ? false : true
    end

    ##
    # Checks whether the actions hash is created on the current jason and creates it in case not
    def check_if_actions_is_created
      @total_configuration[:head][:actions] = {} unless @total_configuration[:head][:actions].is_a?(Hash)
    end

    ##
    # Creates a network request as an action and returns the hash
    # verb : HTTP verb (supported: get, put, post, patch
    # name_of_resource: Action name
    # url: URL to which it will be requested
    # data_hash: data sent to server
    # view: view to which it'll transition
    def create_network_request(verb, name_of_resource, url, data_hash, view)
      check_if_actions_is_created
      @total_configuration[:head][:actions][name_of_resource.to_s] = {
        type: "$network.request",
        options: {url: url, method: verb.downcase, data: data_hash},
        success: {
          type: "href",
          options: {url: @application_url + "/" + view, transtion: "replace"},
        },
        error: {
          type: "$util.toast",
          options: {text: "Something went wrong when #{verb.upcase} on #{url}.", type: "Error"},
        },
      } if ["get", "put", "post", "patch"].include?(verb.downcase)
    end
  end
end
