# frozen_string_literal: true
# The module Josean::Objects implements all the objects available
# as widgets in the jasonette applications
module Josean::Objects
  require "uri"


  ## Creates a switch
  def self.switch(name, value, action, style)
    return unless valid_name(name) && valid_value_switch(value) && valid_action(action) && valid_style(style)
    {
      type: "switch",
      name: name,
      value: value,
      action: {
        trigger: action,
        options: {item: "Name of the switch #{name}"},
      },
    }
  end

  ## Creates a label item for the section[:items]
  # name Name of the label
  # style Hash containing the style
  def self.label(name, style)
    return unless valid_name(name) && valid_style(style)
    {type: "label", text: name, style: style}
  end

  ## Creates a textfield item for the section[:items]
  # name Name of the label
  # style Hash containing the style
  def self.textfield(name, style)
    return unless valid_name(name) && valid_style(style)
    {
      type: "textfield",
      name: name,
      keyboard: "email",
      placeholder: "Enter #{name.downcase}",
      style: style,
    }
  end

  ## Creates a button item for the section[:items]
  # name Name of the label
  # action action name that will be triggered
  # style Hash containing the style
  def self.button(name, url, style)
    return unless valid_name(name) && valid_url(url) && valid_style(style)
    {type: "label", text: name, style: style, href: {url: url}}
  end

  ## Creates an image item for the section[:items]
  # url Url to which it points
  # style Hash containing the style
  def self.image(url, style)
    return unless valid_url(url) && valid_style(style)
    {type: "image", url: url, style: style}
  end

  ## Creates an image item for the section[:items]
  # url Url to which it points
  # style Hash containing the style
  def self.slider(name, value, action, style)
    return unless valid_name(name) && valid_value_slider(value) && valid_action(action) && valid_style(style)
    {
      type: "slider",
      name: name,
      value: value.to_s,
      action: {:"trigger" => action},
    }
  end

  ## Creates a space item for the section[:items]
  # style Hash containing the style
  def self.space(style)
    return unless valid_style_space(style)
    {type: "space", style: style}
  end

  ## Creates a map item for the section[:items]
  # style Hash containing the style
  def self.map(coordinates, width, height, pins, style)
    return unless valid_coordinates(coordinates) && valid_radio(width) && valid_radio(height) && valid_style(style)
    output = {
      type: "map",
      region: {coord: coordinates.join(","), width: width.to_s, height: height.to_s},
      style: style,
    }

    if valid_pins(pins)

      output[:pins] = pins.map do |p|
        pin(p[:title], p[:description], p[:coord], p[:style])
      end
    end

    output
  end

  ## Creates a pin item for a map for the section[:items]
  # style Hash containing the style
  def self.pin(name, description, coordinates, style)
    return unless valid_name(name) && valid_name(description) && valid_coordinates(coordinates) && valid_style(style)
    {
      title: name,
      description: description,
      coord: coordinates.join(","),
      style: style,
    }
  end

  private_class_method def self.valid_name(name)
    name.is_a?(String)

end

  private_class_method def self.valid_style(style)
    style.is_a?(Hash)

end

  private_class_method def self.valid_action(action)
    action.is_a?(Hash)

end

  private_class_method def self.valid_url(url)
    url =~ URI::Parser.new.make_regexp

end

  private_class_method def self.valid_value_switch(value)
    value.is_a?(TrueClass)

end

  private_class_method def self.valid_value_slider(value)
    value.is_a?(Float) || value.is_a?(Integer) && (value >= 0) && (value <= 1)

end

  private_class_method def self.valid_coordinates(coordinates)
    coordinates.is_a?(Array) && coordinates.length == 2 && !(coordinates.map { |coordinate| valid_coordinate(coordinate) }).include?(false)

end

  private_class_method def self.valid_coordinate(coordinate)
    coordinate.is_a?(Float) || coordinate.is_a?(Integer)

end

  private_class_method def self.valid_radio(radio)
    radio.is_a?(Integer) || radio.is_a?(Float) && radio >= 0 && radio <= 100_000

end

  private_class_method def self.valid_pins(pins)
    pins.is_a?(Array) && pins.any? && !(pins.map { |p| valid_pin(p) }).include?(false)

end

  private_class_method def self.valid_pin(pin)
    pin.is_a?(Hash) && (pin.keys & [:coord, :title, :description, :style]).any? && valid_coordinates(pin[:coord]) && valid_style(pin[:style])

end

  private_class_method def self.valid_style_space(style)
    valid_style(style) && (style.keys.map { |k| k.to_s } & ["background", "width", "height"]).any?

end
end
